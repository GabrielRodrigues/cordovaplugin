package cordova.custom.plugin;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Base64;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.sygic.aura.ResourceManager;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;

import cordova.custom.plugin.SygicNaviFragment;
import cordova.custom.plugin.ClassFragment;

/**
 * This class echoes a string called from JavaScript.
 */
public class CordovaCustomPlugin extends CordovaPlugin {

    private static final String DURATION_LONG = "long";
    public static SygicNaviFragment fgm;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if (action.equals("dismiss")) {

            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (cordova.getActivity().getFragmentManager().findFragmentByTag("fragTest") != null) {
                        FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
                        Fragment fragment = cordova.getActivity().getFragmentManager().findFragmentByTag("fragTest");

                        fragmentManager.beginTransaction().remove(fragment).commit();
                    }

                    PluginResult result = new PluginResult(PluginResult.Status.OK, "Tudo Fixeeeee");
                    callbackContext.sendPluginResult(result);


                }
            });
            return true;
        }

        if (action.equals("show")) {
            this.cordova.getActivity().runOnUiThread(new Runnable() {
                @SuppressLint("ResourceType")
                @Override
                public void run() {

                    checkSygicResources(callbackContext);
                }
            });

            /*
            final FrameLayout layout = (FrameLayout) webView.getView().getParent();
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(webViewWidth - left - right, webViewHeight - top - bottom);
            params.setMargins(0, 0, 0, 0);

            Bitmap bmp = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawColor(Color.rgb(250, 0, 0));

            Paint paint = new Paint();

            paint.setColor(Color.BLACK);
            paint.setTextSize(40);
            canvas.drawText("Teste", 30, 100, paint);

            String message;
            String duration;
            PluginResult.Status status = PluginResult.Status.OK;
            String result = BitMapToString(bmp);

            try {
                JSONObject options = args.getJSONObject(0);
                message = options.getString("message") + "Alguma coisa";
                duration = options.getString("duration");
            } catch (JSONException e) {
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }

            // Create the toast
            Toast toast = Toast.makeText(cordova.getActivity(), message ,
                    DURATION_LONG.equals(duration) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
            // Display toast
            toast.show();
            // Send a positive result to the callbackContext
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
            callbackContext.sendPluginResult(pluginResult);*/
            return true;
        }
        return false;
    }

    private void checkSygicResources(CallbackContext callbackContext) {
        ResourceManager resourceManager = new ResourceManager(cordova.getContext(), null);
        if (resourceManager.shouldUpdateResources()) {
            Toast.makeText(cordova.getContext(), "Please wait while Sygic resources are being updated", Toast.LENGTH_LONG).show();
            resourceManager.updateResources(new ResourceManager.OnResultListener() {
                @Override
                public void onSuccess() {

                    initUI(callbackContext);
                }

                @Override
                public void onError(int i, @NonNull String s) {
                    Toast.makeText(cordova.getContext(), (CharSequence) ("Failed to update resources: " + s), i).show();
                }
            });

        } else {
            initUI(callbackContext);
        }

    }

    @SuppressLint("ResourceType")
    private void initUI(CallbackContext callbackContext) {
        int webViewWidth = webView.getView().getWidth();
        int webViewHeight = webView.getView().getHeight();

        fgm = new SygicNaviFragment();

        /*FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(webViewWidth, webViewHeight);
        params.setMargins(50, 50, 50, 50);*/

        FrameLayout layout2 = new FrameLayout(cordova.getContext());
        layout2.setId(11111);

        FrameLayout layout = (FrameLayout) webView.getView().getParent();
        //layout2.setBackgroundColor(Color.YELLOW);

        int marginLeft = (int) (webViewWidth * 0.02);
        int marginTop = (int) ((int) (webViewHeight * 0.195));
        int height = (int) (webViewHeight * 0.77);
        int width = (int) (webViewWidth * 0.467);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
        params.setMargins(marginLeft, marginTop, 0, 0);
        layout2.setLayoutParams(params);

        /*EditText editText = new EditText(cordova.getContext());
        editText.setText("Texto teste");

        layout2.addView(editText);*/

        layout.addView(layout2.getRootView());


        FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();

        fragmentManager.beginTransaction().replace(layout2.getId(), fgm, "fragTest").commitAllowingStateLoss();

        //ClassFragment classFragment = new ClassFragment(layout, fgm, fragmentManager);

        PluginResult result = new PluginResult(PluginResult.Status.OK, "Tudo Fixe");
        callbackContext.sendPluginResult(result);
    }


    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    /*private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }*/


}
