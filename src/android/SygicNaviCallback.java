package cordova.custom.plugin;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.sygic.aura.embedded.IApiCallback;
import com.sygic.sdk.api.ApiDialog;
import com.sygic.sdk.api.ApiItinerary;
import com.sygic.sdk.api.events.ApiEvents;
import com.sygic.sdk.api.exception.GeneralException;

import org.json.JSONException;
import org.json.JSONObject;

public class SygicNaviCallback implements IApiCallback {

    private Activity mActivity;
    public int tmp_waypoints = 2;
    Thread sygicThread = new Thread();

    public SygicNaviCallback(Activity activity) {
        System.out.println("SygicNaviCallBAck");
        mActivity = activity;
    }

    @Override
    public void onEvent(final int event, final String data) {

        if (event == ApiEvents.EVENT_APP_EXIT) {
            mActivity.finish();
        }


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mActivity, "Hello Event", Toast.LENGTH_SHORT).show();
            }
        });

    }


    /*public JSONObject createWaypoint(double lat, double lon, String time1, String time2, int waypointId, String wtype) {
        int ilat = (int) (lat * 100000);
        int ilon = (int) (lon * 100000);

        JSONObject obj = new JSONObject();
        try {
            obj.put("lat", Integer.valueOf(ilat));
            obj.put("lon", Integer.valueOf(ilon));
            obj.put("waypointId", Integer.valueOf(waypointId));
            obj.put("hitRadius", Integer.valueOf(200));
            obj.put("icon", "current_location");
            obj.put("type", wtype);

            if (time1 != null && time2 != null) {
                JSONObject tw = new JSONObject();
                tw.put("startTime", time1);
                tw.put("endTime", time2);
                tw.put("stopDelay", Integer.valueOf(180));

                tw.put("icon", "current_location");
                obj.put("timeWindow", tw);
            }
        } catch (JSONException e) {
            obj = null;
        }

        return obj;
    }*/


    /*public JSONObject createRoutepart(JSONObject wpFrom, JSONObject wpTo) {
        JSONObject obj = new JSONObject();

        try {
            if (wpFrom != null) obj.put("waypointFrom", wpFrom);
            obj.put("waypointTo", wpTo);
        } catch (JSONException e) {
            obj = null;
        }

        return obj;
    }*/


        /*public String CreateJsonItinerary(ArrayList<TwWaypoint> lst) {
            try {
                JSONArray routeparts = new JSONArray();
                JSONObject routepart;
                JSONObject wpFrom;
                JSONObject wpTo;

                for (int i = 1; i < lst.size(); i++) {
                    wpFrom = null;
                    // WayPoint wp  = new WayPoint(233.4, 23.53, "s")
                    if (i == 1)
                        wpFrom = createWaypoint(lst.get(0).lat, lst.get(0).lon, lst.get(0).dtStart, lst.get(0).dtEnd, 0, lst.get(0).wpType);
                    wpTo = createWaypoint(lst.get(i).lat, lst.get(i).lon, lst.get(i).dtStart, lst.get(i).dtEnd, i, lst.get(i).wpType);
                    routepart = createRoutepart(wpFrom, wpTo);
                    if (routepart == null) return null;
                    routeparts.put(routepart);
                }

                JSONObject json = new JSONObject();
                json.put("name", "Bratislava-pickup");
                json.put("version", "2.2");
                json.put("routeParts", routeparts);

                return json.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }*/


    @Override
    public void onServiceConnected() {
        Log.d("SygicCallback", "service connected");
    }

    @Override
    public void onServiceDisconnected() {
        Log.d("SygicCallback", "service disconnected");
    }

}
