package cordova.custom.plugin;

import com.sygic.aura.embedded.SygicFragment;
import cordova.custom.plugin.SygicNaviCallback;

public class SygicNaviFragment extends SygicFragment {

    @Override
    public void onResume() {
        System.out.println("SygicNaviFragment");
        startNavi();
        setCallback(new SygicNaviCallback(getActivity()));
        super.onResume();
    }
}