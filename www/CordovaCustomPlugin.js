function CordovaCustomPlugin() {}

CordovaCustomPlugin.prototype.show = function (message, duration, success, error) {
    var options = {};
    options.message = message;
    options.duration = duration;
    cordova.exec(success, error, 'CordovaCustomPlugin', 'show', [options]);
};

CordovaCustomPlugin.prototype.dismiss = function (message, duration, success, error) {
    var options = {};
    options.message = message;
    options.duration = duration;
    cordova.exec(success, error, 'CordovaCustomPlugin', 'dismiss', [options]);
};

CordovaCustomPlugin.install = function() {
    if (!window.plugins) {
        window.plugins = {};
    }
    window.plugins.cordovaCustomPlugin = new CordovaCustomPlugin();
    return window.plugins.cordovaCustomPlugin;
};
cordova.addConstructor(CordovaCustomPlugin.install);

/*var exec = require('cordova/exec');

module.exports={

    dismiss: function (message, duration, success, error) {

        var options = {};
        options.message = message;
        options.duration = duration;

        exec(success, error, 'CordovaCustomPlugin', 'dismiss', [options]);
    },

    show: function (message, duration, success, error) {

        var options = {};
        options.message = message;
        options.duration = duration;

        exec(success, error, 'CordovaCustomPlugin', 'show', [options]);
    }

}*/
